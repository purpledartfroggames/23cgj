﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotCollision : MonoBehaviour
{
    [SerializeField] GameObject CollectGO;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Crate" && CollectGO.transform.childCount == 0)
        {
            CrateController MyCrateController = collision.gameObject.GetComponent<CrateController>();

            if (MyCrateController.IsPickable() == true)
            {
                AudioManager.Instance.PlaySoundInteraction("CratePickup");

                collision.gameObject.transform.parent = CollectGO.transform;
                collision.gameObject.transform.position = CollectGO.transform.position;
                collision.gameObject.transform.localScale = new Vector3(.6f, .6f, .6f);
                MyCrateController.SetPickedUpByPlayer();
            }
        }
        else if (collision.tag == "DropPoint" && CollectGO.transform.childCount == 1)
        {
            if (CollectGO.transform.GetChild(0).GetComponent<CrateController>().IsCrateLethal() == true)
            {
                GameManager.Instance.AdjustNumOfHearts(-1);
            }

            if (CollectGO.transform.GetChild(0).GetComponent<CrateController>().GetCrateType() != QueueManager.Instance.EnumFromQueueAt(0)
                 || CollectGO.transform.GetChild(0).GetComponent<CrateController>().IsCrateDangerous() == true)
            {
                AudioManager.Instance.PlaySoundInteraction("RobotPoison");
                CollectGO.transform.GetChild(0).gameObject.GetComponent<CrateController>().DoRemoveCrate(collision.transform);
                QueueManager.Instance.RemoveFromQueue();
                QueueManager.Instance.AddToQueue((EnumCrateType)Random.Range(0, (int)EnumCrateType.COUNT), false);
                QueueManager.Instance.AddToQueue((EnumCrateType)Random.Range(0, (int)EnumCrateType.COUNT), false);
                GameManager.Instance.AdjustNumOfToxicity(1);
            }
            else if (CollectGO.transform.GetChild(0).GetComponent<CrateController>().GetCrateType() == QueueManager.Instance.EnumFromQueueAt(0))
            {
                AudioManager.Instance.PlaySoundInteraction("CrateDrop");
                CollectGO.transform.GetChild(0).gameObject.GetComponent<CrateController>().DoRemoveCrate(collision.transform);
                QueueManager.Instance.RemoveFromQueue();
            }
        }
        else if (collision.tag == "DropPointIncinerator" || CollectGO.transform.childCount > 1)
        {
            foreach (Transform curTransform in CollectGO.transform)
            {
                if (curTransform.gameObject.GetComponent<CrateController>().IsCrateDangerous())
                {
                    AudioManager.Instance.PlaySoundInteraction("RobotToxicityLower");
                    GameManager.Instance.AdjustNumOfToxicity(-1);
                }

                if (curTransform.gameObject.GetComponent<CrateController>().IsCrateLethal())
                {
                    AudioManager.Instance.PlaySoundInteraction("RobotHeartUp");
                    GameManager.Instance.AdjustNumOfHearts(1);
                }

                AudioManager.Instance.PlaySoundInteraction("CrateIncinerate");
                Destroy(curTransform.gameObject);
            }
        }
    }
}
