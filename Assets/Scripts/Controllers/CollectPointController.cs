﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectPointController : MonoBehaviour
{
    [SerializeField] GameObject CollectPointGroup;
    [SerializeField] GameObject CratePrefab;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (CollectPointGroup.transform.childCount < 1)
        {
            GameObject CreatedCrate;

            CreatedCrate = Instantiate(CratePrefab);
            CreatedCrate.transform.position = CollectPointGroup.transform.position;
            CreatedCrate.transform.parent = CollectPointGroup.transform;
        }
    }
}
