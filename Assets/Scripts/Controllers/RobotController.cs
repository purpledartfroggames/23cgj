﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotController : MonoBehaviour
{
    [SerializeField] Rigidbody2D myRigibody2D;
    [SerializeField] GameObject MyGraphics;
    [SerializeField] float MovementForce;
    [SerializeField] float MaxMovementForce;
    [SerializeField] float JumpForce;
    [SerializeField] Transform JumpPointRobot;
    [SerializeField] Transform JumpPointCarrier;


    bool IsGrounded;
    bool IsDead = false;

    private void Start()
    {
        GameManager.Instance.GetMenuController().ActivatePanelGameHelp();
        AudioManager.Instance.PlaySoundTheme("Theme");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.M))
            AudioManager.Instance.PlaySoundTheme("Theme");
        else if (Input.GetKeyDown(KeyCode.H))
            GameManager.Instance.GetMenuController().ActivatePanelGameHelp();
        else if (Input.GetKeyDown(KeyCode.R))
            GameManager.Instance.GetMenuController().RestartGame();
        else if (Input.GetKeyDown(KeyCode.P))
            GameManager.Instance.TogglePause();
        else if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
            GameManager.Instance.GetMenuController().DeactivateAllPanels();

        if (IsDead == false && Time.timeScale != 0f)
        {
            this.SetIsGrounded();
            this.MoveOnBelt();
            this.JumpBelt();
            this.CapBeltSpeed();
        }
    }

    void MoveOnBelt()
    {
        Vector2 InputForce;

        InputForce = new Vector2(Input.GetAxisRaw("Horizontal"), 0f);

        if (InputForce.x < 0.0f)
        {
            MyGraphics.transform.localScale = new Vector3(-1f, 1f, 1f);
        }
        else if (InputForce.x > 0.0f)
        {
            MyGraphics.transform.localScale = new Vector3(1f, 1f, 1f);
        }

        if (IsGrounded == false)
        {
            InputForce = InputForce / 5f;
        }

        if (InputForce.x != 0f)
        {
            AudioManager.Instance.PlaySoundInteraction("RobotMove");
        }
        else
        {
            AudioManager.Instance.StopPlaySoundInteraction("RobotMove");
        }

        myRigibody2D.AddForce(InputForce * MovementForce, ForceMode2D.Impulse);
    }

    void CapBeltSpeed()
    {
        if (myRigibody2D.velocity.x < 0f)
        {
            myRigibody2D.velocity = new Vector3(Mathf.Min(MaxMovementForce, myRigibody2D.velocity.x), myRigibody2D.velocity.y);
        }
        else
        {
            myRigibody2D.velocity = new Vector3(Mathf.Max(-MaxMovementForce, myRigibody2D.velocity.x), myRigibody2D.velocity.y);
        }

    }

    void JumpBelt()
    {
        if (IsGrounded && Input.GetKeyDown(KeyCode.Space))
        {
            myRigibody2D.AddForce(transform.up * JumpForce);
            AudioManager.Instance.PlaySoundInteraction("RobotJump");
        }
    }

    void SetIsGrounded()
    {
        RaycastHit2D[] hitInfoLeft;
        RaycastHit2D[] hitInfoRight;

        bool didHitGround = false;

        hitInfoLeft = Physics2D.RaycastAll(JumpPointRobot.position, JumpPointRobot.up * -1f, .1f);
        Debug.DrawRay(JumpPointRobot.position, JumpPointRobot.up * -.1f, Color.red);

        hitInfoRight = Physics2D.RaycastAll(JumpPointCarrier.position, JumpPointRobot.up * -1f, .1f);
        Debug.DrawRay(JumpPointCarrier.position, JumpPointRobot.up * -.1f, Color.green);

        didHitGround = didHitGround || this.IsCollideGround(hitInfoLeft);
        didHitGround = didHitGround || this.IsCollideGround(hitInfoRight);

        IsGrounded = didHitGround;
    }

    bool IsCollideGround(RaycastHit2D[] raycastHit)
    {
        bool CollideGround = false;

        foreach (RaycastHit2D hit in raycastHit)
        {
            if (hit.collider.tag == "Ground")
            {
                CollideGround = true;
                break;
            }
        }
        return CollideGround;
    }

    public void SetDead()
    {
        myRigibody2D.velocity = Vector3.zero;
        MyGraphics.transform.localScale = new Vector3(-1f, -1f, 1f);
        IsDead = true;
    }
}
