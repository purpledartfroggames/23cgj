﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CrateController : MonoBehaviour
{
    EnumCrateType MyCrateType;
    EnumCrateType MyLyingCrateType;

    bool IsDangerous;
    bool IsLethal;
    bool SystemLying;
    
    [SerializeField] SpriteRenderer MySpriteRenderer;
    [SerializeField] SpriteRenderer MySpriteRendererLabel;
    [SerializeField] GameObject MySpriteRendererDangerousGO;
    [SerializeField] GameObject MySpriteRendererLethalGO;

    [SerializeField] float SpoilTime;
    [SerializeField] TextMeshProUGUI TimerText;

    bool PickedUpByPlayer = false;
    Vector3 distanceTravelled;
    bool RemoveCrate = false;


    // Start is called before the first frame update
    void Start()
    {
        SystemLying = Random.Range(0, 5) == 1 ? true : false; //TODO: definately LOWER!!!

        do
        {
            MyLyingCrateType = (EnumCrateType)Random.Range(0, (int)EnumCrateType.COUNT);
        }
        while (MyLyingCrateType == MyCrateType);

        if (Random.Range(0, 3) == 2)
        {
            if (QueueManager.Instance.IndexRelevantInQueue(0))
                MyCrateType = QueueManager.Instance.EnumFromQueueAt(0);
        }
        else
        {
            MyCrateType = (EnumCrateType)Random.Range(0, (int)EnumCrateType.COUNT);
        }

        MySpriteRenderer.sprite = GameManager.Instance.GetMenuController().SpriteFromEnum(MyCrateType); //TODO: Ugly place!!!!
        MySpriteRendererLabel.sprite = GameManager.Instance.GetMenuController().SpriteLabelFromEnum(MyCrateType); //TODO: Ugly place!!!!

        if (SystemLying == true)
        {
            MySpriteRendererLabel.sprite = GameManager.Instance.GetMenuController().SpriteLabelFromEnum(MyLyingCrateType); //TODO: Ugly place!!!!
        }

        IsDangerous = Random.Range(0, 4) == 1 ? true : false;
        IsLethal = Random.Range(0, 7) == 1 ? true : false;

        MySpriteRendererDangerousGO.SetActive(IsDangerous);
        MySpriteRendererLethalGO.SetActive(IsLethal);

        SpoilTime = Mathf.Max(0f, Random.Range(SpoilTime - 2f, SpoilTime + 2f));
        Invoke("DestroyCrate", SpoilTime);
    }

    public EnumCrateType GetCrateType()
    {
        return MyCrateType;
    }

    private void FixedUpdate()
    {
        SpoilTime -= Time.deltaTime;

        if (PickedUpByPlayer == true || RemoveCrate == true)
            TimerText.text = "-";
        else
            TimerText.text = ((int)SpoilTime).ToString();

        if (PickedUpByPlayer == false)
        {
            if (distanceTravelled.x <= 1f)
            {
                Vector3 toMove = new Vector3(.6f, 0f, 0f) * Time.deltaTime;
                distanceTravelled += toMove;
                this.transform.Translate(toMove);
            }

            if (RemoveCrate == true)
            {
                Vector3 toMove = new Vector3(-.8f, 0f, 0f) * Time.deltaTime;
                this.transform.Translate(toMove);
            }
        }
    }

    private void DestroyCrate()
    {
        if (this.gameObject != null && PickedUpByPlayer == false) 
        {
            RemoveCrate = true;
            Destroy(this.gameObject, 3f);
        }
    }

    public void DoRemoveCrate(Transform _dropPoint)
    {
        this.transform.position = _dropPoint.position;
        this.transform.localScale = new Vector3(1.5f, 1.5f, 1f);
        this.transform.parent = null;

        if (this.gameObject != null)
        {
            PickedUpByPlayer = false;
            RemoveCrate = true;
            Destroy(this.gameObject, 3f);
        }
    }

    public void SetPickedUpByPlayer()
    {
        PickedUpByPlayer = true;
    }

    public bool IsCrateLethal()
    {
        return IsLethal;
    }

    public bool IsCrateDangerous()
    {
        return IsDangerous;
    }

    public bool IsPickable()
    {
        return PickedUpByPlayer == false && RemoveCrate == false;
    }
}
