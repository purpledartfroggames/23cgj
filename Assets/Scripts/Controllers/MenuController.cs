﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class MenuController : MonoBehaviour
{
    [SerializeField] Image CrateQueue1;
    [SerializeField] Image CrateQueue2;
    [SerializeField] Image CrateQueue3;
    [SerializeField] Image CrateQueue4;
    [SerializeField] Image CrateQueue5;

    [SerializeField] Sprite CrateBats;
    [SerializeField] Sprite CrateFishies;
    [SerializeField] Sprite CrateGopplers;
    [SerializeField] Sprite CrateWorms;
    [SerializeField] Sprite CrateUnassigned;

    [SerializeField] Sprite LabelCrateBats;
    [SerializeField] Sprite LabelCrateFishies;
    [SerializeField] Sprite LabelCrateGopplers;
    [SerializeField] Sprite LabelCrateWorms;

    [SerializeField] GameObject GameWonPanel;
    [SerializeField] GameObject GameLostPanel;
    [SerializeField] GameObject GameHelpPanel;

    [SerializeField] Transform HeartsGroup;
    [SerializeField] Transform ToxicityGroup;

    [SerializeField] TextMeshProUGUI ProgressText;

    private void Update()
    {
        int NumOfHearts = GameManager.Instance.CurHearts();
        int NumOfToxic = GameManager.Instance.CurToxicity(); ;

        HeartsGroup.GetChild(0).gameObject.SetActive(NumOfHearts >= 1);
        HeartsGroup.GetChild(1).gameObject.SetActive(NumOfHearts >= 2);
        HeartsGroup.GetChild(2).gameObject.SetActive(NumOfHearts >= 3);

        ToxicityGroup.GetChild(0).gameObject.SetActive(NumOfToxic >= 1);
        ToxicityGroup.GetChild(1).gameObject.SetActive(NumOfToxic >= 2);
        ToxicityGroup.GetChild(2).gameObject.SetActive(NumOfToxic >= 3);

        ProgressText.text = QueueManager.Instance.CratesLeft().ToString() + " Crates Left";
    }

    public void UpdateInterface()
    {
        if (QueueManager.Instance != null)
        {
            CrateQueue1.sprite = QueueManager.Instance.IndexRelevantInQueue(0) ? this.SpriteLabelFromEnum(QueueManager.Instance.EnumFromQueueAt(0)) : CrateUnassigned;
            CrateQueue2.sprite = QueueManager.Instance.IndexRelevantInQueue(1) ? this.SpriteLabelFromEnum(QueueManager.Instance.EnumFromQueueAt(1)) : CrateUnassigned;
            CrateQueue3.sprite = QueueManager.Instance.IndexRelevantInQueue(2) ? this.SpriteLabelFromEnum(QueueManager.Instance.EnumFromQueueAt(2)) : CrateUnassigned;
            CrateQueue4.sprite = QueueManager.Instance.IndexRelevantInQueue(3) ? this.SpriteLabelFromEnum(QueueManager.Instance.EnumFromQueueAt(3)) : CrateUnassigned;
            CrateQueue5.sprite = QueueManager.Instance.IndexRelevantInQueue(4) ? this.SpriteLabelFromEnum(QueueManager.Instance.EnumFromQueueAt(4)) : CrateUnassigned;
        }
    }

    public Sprite SpriteFromEnum(EnumCrateType _curCrateType)
    {
        Sprite curSprite = CrateUnassigned;

        switch (_curCrateType)
        {
            case EnumCrateType.CrateBats:
                curSprite = CrateBats;
                break;
            case EnumCrateType.CrateFishes:
                curSprite = CrateFishies;
                break;
            case EnumCrateType.CrateGopplers:
                curSprite = CrateGopplers;
                break;
            case EnumCrateType.CrateWorms:
                curSprite = CrateWorms;
                break;
        }

        return curSprite;
    }

    public Sprite SpriteLabelFromEnum(EnumCrateType _curCrateType)
    {
        Sprite curSprite = CrateUnassigned;

        switch (_curCrateType)
        {
            case EnumCrateType.CrateBats:
                curSprite = LabelCrateBats;
                break;
            case EnumCrateType.CrateFishes:
                curSprite = LabelCrateFishies;
                break;
            case EnumCrateType.CrateGopplers:
                curSprite = LabelCrateGopplers;
                break;
            case EnumCrateType.CrateWorms:
                curSprite = LabelCrateWorms;
                break;
        }

        return curSprite;
    }

    public void DeactivateAllPanels()
    {
        GameWonPanel.SetActive(false);
        GameLostPanel.SetActive(false);
        GameHelpPanel.SetActive(false);
    }

    public void ActivatePanelGameWon()
    {
        this.DeactivateAllPanels();
        GameWonPanel.SetActive(true);
    }

    public void ActivatePanelGameLost()
    {
        this.DeactivateAllPanels();
        GameLostPanel.SetActive(true);
    }

    public void ActivatePanelGameHelp()
    {
        this.DeactivateAllPanels();
        GameHelpPanel.SetActive(true);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(0);
    }
}
