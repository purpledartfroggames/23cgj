﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeltFlipper : MonoBehaviour
{
    [SerializeField] float FlipTime;
    [SerializeField] Transform GfxToFlip;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("Flip", 0f, FlipTime);
    }

    void Flip()
    {
        GfxToFlip.localScale = new Vector3(GfxToFlip.localScale.x * -1f, GfxToFlip.localScale.y, GfxToFlip.localScale.z);
    }
}
