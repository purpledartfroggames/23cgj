﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class QueueManager : MonoBehaviour
{
    static public QueueManager Instance;

    [SerializeField] Sprite unspecified;
    [SerializeField] int NumOfCrates;

    [SerializeField] List<EnumCrateType> CratesQueue;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;

            this.InitQueue();
            GameManager.Instance.GetMenuController().UpdateInterface();
        }
        else
        {
            Destroy(this);
            return;
        }
    }

    private void Update()
    {
        if (CratesQueue.Count == 0)
        {
            this.GameWonNotifyGameManager();
        }
    }

    private void InitQueue()
    {
        for (int i = 0; i < NumOfCrates; i++)
        {
            this.AddToQueue((EnumCrateType)Random.Range(0, (int)EnumCrateType.COUNT), false);
        }
    }

    public void AddToQueue(EnumCrateType _newRequest, bool _updateMenu)
    {
        CratesQueue.Add(_newRequest);

        if (_updateMenu == true)
        {
            GameManager.Instance.GetMenuController().UpdateInterface();
        }
    }

    public void RemoveFromQueue()
    {
        CratesQueue.RemoveAt(0);
        GameManager.Instance.GetMenuController().UpdateInterface();
    }

    public EnumCrateType EnumFromQueueAt(int _crateIndex)
    {
        EnumCrateType curCrateType;

       curCrateType = CratesQueue.ElementAt(_crateIndex);

        return curCrateType;
    }

    public bool IndexRelevantInQueue(int _crateIndex)
    {
        bool isIndexRelevant = false;

        if (CratesQueue.Count >= _crateIndex + 1)
            isIndexRelevant = true;

        return isIndexRelevant;
    }

    private void GameWonNotifyGameManager()
    {
        GameManager.Instance.GameWon();
    }

    public int CratesLeft()
    {
        return CratesQueue.Count;
    }
}
