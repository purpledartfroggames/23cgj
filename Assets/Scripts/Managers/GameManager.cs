﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    static public GameManager Instance;

    [SerializeField] MenuController MyMenuController;

    [SerializeField] int NumOfHearts;
    [SerializeField] int NumOfToxicLevels;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
            return;
        }
    }

    private void Update()
    {
        if (NumOfHearts <= 0 || NumOfToxicLevels >= 3)
            this.GameLost();
    }

    public MenuController GetMenuController()
    {
        return MyMenuController;
    }

    public void GameWon()
    {
        GameManager.Instance.GetMenuController().ActivatePanelGameWon();
    }

    public void GameLost()
    {
        GameManager.Instance.GetMenuController().ActivatePanelGameLost();
        GameObject.FindGameObjectWithTag("Player").GetComponent<RobotController>().SetDead();
    }

    public int CurHearts()
    {
        return NumOfHearts;
    }

    public int CurToxicity()
    {
        return NumOfToxicLevels;
    }

    public void AdjustNumOfHearts(int _adjustment)
    {
        NumOfHearts = Mathf.Clamp(NumOfHearts + _adjustment, 0, 3);
    }

    public void AdjustNumOfToxicity(int _adjustment)
    {
        NumOfToxicLevels = Mathf.Clamp(NumOfToxicLevels + _adjustment, 0, 3);
    }

    public void TogglePause()
    {
        if (Time.timeScale == 0f)
            Time.timeScale = 1f;
        else
            Time.timeScale = 0f;
    }

}
